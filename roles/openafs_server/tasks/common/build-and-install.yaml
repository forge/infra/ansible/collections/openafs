---
#
# Install OpenAFS from source.
#
# The checkout method determines how the source code is retrieved for the build.
#
# We build both the server and client binaries (including the kernel module) if
# the node is both a client and a server. This is to avoid mismatch shared
# libraries and binaries, since this node is installed from source.
#
# Currently, we a are limited to systemd and do not support selinux enforcing
# mode when installing from source.
#
# The afs_configure_options host variable may be set to override the default
# configure options used in the build.
#

# Install tools first to ensure git is installed too.
- name: Ensure development tools are installed
  import_role:
    name: openafs_contrib.openafs.openafs_devel

- name: Verify service manager type is supported
  assert:
    that: ansible_service_mgr in ["systemd"]

- name: Verify selinux is disabled
  assert:
    that: ansible_selinux.mode != 'enforcing'
  when:
    - ansible_selinux is defined
    - ansible_selinux | type_debug == 'dict'
    - ansible_selinux.status == 'enabled'

- include_role:
    name: openafs_contrib.openafs.openafs_common
    tasks_from: "checkout_{{ checkout_method }}"

- debug:
    var: checkout_results.changed

- name: Reset build completed fact
  become: yes
  openafs_contrib.openafs.openafs_store_facts:
    state: update
    facts:
      build_completed: no
  when: checkout_results.changed

- include_tasks: "{{ role_path }}/tasks/common/build-{{ afs_is_client | ternary('all', 'bins') }}.yaml"
  when: >
    (afs_always_build | bool) or
    ansible_local.openafs.build_completed is undefined or
    not (ansible_local.openafs.build_completed | bool)

- name: Set build completed fact
  become: yes
  openafs_contrib.openafs.openafs_store_facts:
    state: update
    facts:
      build_completed: yes

  when: checkout_results.changed
- name: Install
  when:
    - build_results is defined
    - build_results.changed
  block:
    - name: Ensure OpenAFS client is stopped
      become: yes
      service:
        name: "{{ ansible_local.openafs.client_service_name }}"
        state: stopped
      when:
        - afs_is_client
        - ansible_local.openafs.client_service_name is defined

    - name: Ensure OpenAFS servers are stopped
      become: yes
      service:
        name: "{{ ansible_local.openafs.server_service_name }}"
        state: stopped
      when:
        - ansible_local.openafs.server_service_name is defined

    - name: Install OpenAFS binaries
      become: yes
      openafs_contrib.openafs.openafs_install_bdist:
        path: "{{ ansible_local.openafs.destdir }}"
      register: install_results

    - debug:
        var: install_results

    - name: Store installation facts
      become: yes
      openafs_contrib.openafs.openafs_store_facts:
        state: update
        facts:
          server_service_name: openafs-server
          server_installed: yes
          client_installed: "{{ afs_is_client }}"
          bins: "{{ install_results.bins }}"
          dirs: "{{ install_results.dirs }}"
      when:
        - install_results.changed
        - not ansible_check_mode

# Currently, we a are limited to systemd when installing from source.
- name: Install systemd unit file
  become: yes
  template:
    src: openafs-server.service.j2
    dest: "/etc/systemd/system/{{ afs_server_service_name }}.service"
  register: unit_file

- name: Reload systemd
  become: yes
  systemd:
    daemon_reload: yes
  when: unit_file.changed
